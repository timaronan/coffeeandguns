function init() {

	var northWest = new L.latLng(72.5017, -180),
	    southEast = new L.latLng(-55.5469,10.8333),
	    centeroid = new L.latLng(39.5040,-96.416),
	    bounds = new L.latLngBounds(northWest, southEast);
	// initialize map object with view
	var map = new L.Map('mapLayered', {
		center: centeroid,
		zoom: 5,
		minZoom: 3,
		maxZoom: 7,
		maxBounds: bounds
	});
	new L.Control.Zoom({ position: 'bottomright' }).addTo(map);
  	var totStarbucks = 10843;
  	var totGunshops = 64747;
  	var feature;
	map.setMaxBounds(bounds);
	var tip = d3.tip()
		.attr('class', 'd3-tip map')
		.offset([0, 0])
		.html(function(d) {
			return "<span class='at'>"+d.properties.city+","+d.properties.state+"</span><span class='row mass'><span class='head'>Mass Shootings</span><span class='num'>"+d.properties.tot+"</span><span class='row guns'><span class='head'>Gunshops</span><span class='num'>"+d.properties.gunshops+"</span><span class='row coffee'><span class='head'>Starbucks</span><span class='num'>"+d.properties.starbucks+"</span>";
		});
	// add tile layer
	var bg = L.tileLayer('tiles/bg/{z}/{x}/{y}.png', { errorTileUrl:'empty.png' }).addTo(map);
	var guns = L.tileLayer('tiles/guns/{z}/{x}/{y}.png', { errorTileUrl:'empty.png' }).addTo(map);
	var coffee = L.tileLayer('tiles/coffee/{z}/{x}/{y}.png', { errorTileUrl:'empty.png' }).addTo(map);
	// var shootings = L.tileLayer('tiles/shootings/{z}/{x}/{y}.png', { errorTileUrl:'empty.png' }).addTo(map);
	d3.select(guns.getContainer()).classed('guns', true);
	d3.select(coffee.getContainer()).classed('coffee', true);

	var mapcont = d3.select('#mapLayered');
	var btncont = mapcont.append('div').attr('class', 'btns');
	mapcont.classed(["guns coffee mass"], true);
	var gunbtn = btncont.append('div').attr('class', 'btn guns');
	gunbtn.on('click', function(){
			mapcont.classed("guns", !mapcont.classed("guns"));
		})
		.text('Gun Shops');
	var massbtn = btncont.append('div').attr('class', 'btn mass');
	massbtn.on('click', function(){
			mapcont.classed("mass", !mapcont.classed("mass"));
		})
		.text('Mass Shootings');		
	var coffeebtn = btncont.append('div').attr('class', 'btn coffee');
	coffeebtn.on('click', function(){
			mapcont.classed("coffee", !mapcont.classed("coffee"));
		})
		.text('Starbucks');  		

	map.scrollWheelZoom.disable();
	map._initPathRoot();

	svgOverlay();
	function svgOverlay(){
		var data = geojson.features.sort(function(a, b) { return b.properties.tot - a.properties.tot; });
		/* Initialize the SVG layer */
		var svg = d3.select("#mapLayered").select(".leaflet-objects-pane svg"),
		g = svg.append("g");
		svg.call(tip);
		geojson.features.forEach(function(d) {
			d.LatLng = new L.LatLng(d.geometry.coordinates[1],
									d.geometry.coordinates[0])	
			// console.log(d.LatLng);
		});
		console.log(data);
		feature = g.selectAll("circle")
			.data(data)
			.enter()
			.append("circle")
			.attr("class", function(d,i){    		    	
		    	return d.properties.city+","+d.properties.state;
			})
			.attr("r", function(d,i){
				return d.properties.tot * 2;
			});  
		feature.on("mouseover", tip.show)
		.on("mouseleave", tip.hide);
		update();
		map.on("viewreset", update);
	}
	function update() {
		feature.attr("transform", function(d) { 
			return "translate("+ 
				map.latLngToLayerPoint(d.LatLng).x +","+ 
				map.latLngToLayerPoint(d.LatLng).y +")";
			})
		.attr("r", function(d,i){
			return d.properties.tot * 2;		
		});
	}


	d3.select("#why").on("click", function(){
		var bec = d3.select("footer");
		var clac = bec.classed("active");
		var txt;
		if(clac){
			txt = "Why did we do this?";
			console.log("clac");
		}else{
			txt = "CLOSE";
		}
		bec.classed("active", !clac);
		d3.select(this).text(txt);
	});
  	voronoiLogo();
	function voronoiLogo(){
	  var letters = [{"name" : "point-p" , "d" : "M373.3,73.6c0-1.9,1.1-3,3-3h44.9c17.6,0,26.9,9.3,26.9,26.9v45.6c0,17.4-9.3,26.9-26.9,26.9h-22.4v47.1c0,1.9-0.9,3-3,3 h-19.4c-1.9,0-3-1.1-3-3V73.6H373.3z M415.2,145.4c5,0,7.5-2.8,7.5-7.5v-35.1c0-4.9-2.4-7.5-7.5-7.5h-16.4v50.1H415.2z"},{"name" : "point-o" , "d" : "M476.1,97.6c0-17.6,9.3-26.9,26.9-26.9h21c17.6,0,26.9,9.3,26.9,26.9v95.7c0,17.6-9.3,26.9-26.9,26.9h-21 c-17.6,0-26.9-9.3-26.9-26.9V97.6z M519.3,196.1c4.9,0,7.5-2.4,7.5-7.5v-86.4c0-4.9-2.6-7.5-7.5-7.5h-10.8c-4.9,0-7.5,2.6-7.5,7.5 v86.3c0,5,2.6,7.5,7.5,7.5h10.8V196.1z"},{"name" : "point-i" , "d" : "M601.3,70.6c1.9,0,3,1.1,3,3v143.5c0,1.9-1.1,3-3,3h-19.4c-2.1,0-3-1.1-3-3V73.6c0-1.9,0.9-3,3-3H601.3z"},{"name" : "point-n" , "d" : "M709.2,70.6c1.9,0,3,1.1,3,3v143.5c0,1.9-1.1,3-3,3h-18.5c-1.7,0-2.8-0.7-3.4-2.4l-28.6-79.8h-0.9v79.2c0,1.9-0.9,3-3,3 h-19.4c-1.9,0-3-1.1-3-3V73.6c0-1.9,1.1-3,3-3h18.5c1.7,0,2.8,0.7,3.4,2.4l28.4,79.4h1.1V73.6c0-1.9,0.9-3,3-3H709.2L709.2,70.6z"},{"name" : "point-t" , "d" : "M808.2,70.6c2.1,0,3,1.1,3,3v18.7c0,1.9-0.9,3-3,3h-21.7v121.9c0,2.1-0.9,3-3,3h-19.4c-1.9,0-3-0.9-3-3V95.3h-21.7 c-1.9,0-3-1.1-3-3V73.6c0-1.9,1.1-3,3-3H808.2L808.2,70.6z"},{"name" : "interactive-i-1" , "d" : "M319.2,284v-25.6h4.5V284H319.2z"},{"name" : "interactive-n" , "d" : "M373,284l-13.3-18.3V284h-4.5v-25.6h4.6l13.1,17.7v-17.7h4.5V284H373z"},{"name" : "interactive-t-1" , "d" : "M415,284v-21.6h-7.7v-3.9h20v3.9h-7.7V284H415z"},{"name" : "interactive-e-1" , "d" : "M457.1,284v-25.6h17.5v3.9h-13v6.6h12.8v3.9h-12.8v7.1h13v3.9h-17.5V284z"},{"name" : "interactive-r" , "d" :"M519.9,284l-5.6-9.5h-4.5v9.5h-4.5v-25.6h11.2c5.1,0,8.4,3.3,8.4,8c0,4.5-3,7-6.1,7.5l6.3,10h-5.2L519.9,284z M520.3,266.4 c0-2.5-1.8-4-4.4-4h-6.2v8.1h6.2C518.4,270.5,520.3,268.9,520.3,266.4z"},{"name" : "interactive-a" , "d" : "M573.4,284l-1.9-4.9h-11.7l-1.9,4.9h-5.1l10-25.6h5.6l10,25.6H573.4z M565.6,262.9l-4.6,12.2h9.2L565.6,262.9z"},{"name" : "interactive-c" , "d" : "M604.8,271.2c0-7.9,5.9-13.2,13.4-13.2c5.1,0,8.4,2.6,10.2,5.7l-3.8,2c-1.2-2.1-3.7-3.7-6.4-3.7c-5.1,0-8.8,3.8-8.8,9.2 c0,5.3,3.8,9.2,8.8,9.2c2.7,0,5.2-1.6,6.4-3.7l3.8,1.9c-2,3.1-5.1,5.8-10.3,5.8C610.7,284.4,604.8,279.1,604.8,271.2L604.8,271.2z"},{"name" : "interactive-t-2" , "d" : "M664.3,284v-21.6h-7.7v-3.9h20v3.9h-7.7V284H664.3z"},{"name" : "interactive-i-2" , "d" : "M706.4,284v-25.6h4.5V284H706.4z"},{"name" : "interactive-v" , "d" : "M749.8,284l-10-25.6h5.1l7.7,20.7l7.7-20.7h5.1l-10,25.6H749.8z"},{"name" : "interactive-e-2" , "d" : "M794.2,284v-25.6h17.5v3.9h-13v6.6h12.8v3.9h-12.8v7.1h13v3.9h-17.5V284z" }];

	  var numbers = [{"id" : "one", "d" : "M841,286c-0.8-2.4-2.4-4.8-2.4-7.3c-0.2-25.3,0-50.7-0.2-76c0-5.6,2.4-8.9,7.1-11.5c20-10.6,39.8-21.5,59.9-32.1 c2.9-1.6,4-3.3,3.9-6.6c-0.2-11.5,0-23-0.1-34.5c0-8-3.2-11.4-11-11.6c-4.5-0.1-9,0-13.5,0.2c-6.3,0.4-9.4,3.4-9.6,9.8 c-0.2,6.7-0.2,13.3,0,20c0.1,4.3-1.6,6-5.9,6c-8.2-0.1-16.3,0-24.5,0.2c-4.4,0.1-6.3-1.7-6.2-6.2c0.2-10.7-0.4-21.4,0.4-32 c1.5-20.1,11-30.8,31-32.8c14.8-1.5,30.1-1.5,44.9,0c20.2,2.1,30.6,14.1,31.1,34.4c0.6,22.3,0.1,44.7,0.2,67 c0,5.4-3.5,7.9-7.6,10.1c-16,8.6-32,17.1-47.9,25.9c-18.2,10.1-15.1,4.7-15.4,25.6c0,3.2,0.1,6.3,0,9.5c-0.1,4.2,2,6.2,6,6.3 c9.1,0.1,18.3,0,28.1,0c0-3.6,0.1-6.7,0-9.8c-0.1-4.5,1.9-6.8,6.6-6.7c8,0.1,16,0,24,0c4.1,0,6.2,1.9,6.2,6.1 c0,13.2,0.2,26.3,0,39.5c0,2.2-1.9,4.3-2.9,6.5C909,286,875,286,841,286z"},{"id" : "two", "d" : "M992,286c-0.9-2.7-2.5-5.4-2.5-8.1c-0.2-52.6-0.1-105.3-0.1-157.9c0-2.5,0-4.9,0-8c-5.6,0.7-10.7,1.3-15.7,1.9 c-4.5,0.6-6.5-1.4-6.5-5.9c0-8,0-16,0-24c0-3.3,1.1-5.8,4.9-6.3c16.2-2.2,32.3-4.5,48.5-6.6c1.7-0.2,3.6,1.1,5.4,1.7 c0,71,0,142,0,213C1014.7,286,1003.3,286,992,286z"},{"id" : "one21", "d" : "M319.6,100.6c-3.8,0.4-7.1,0.7-10.3,1.2c-3.7,0.5-5.3-0.9-5.3-4.6c0.1-5.3-0.3-10.7,0.2-16c0.2-1.7,2.3-4.3,3.8-4.5 c10.8-1.8,21.7-3.2,32.6-4.7c4.3-0.6,4.5,2.1,4.5,5.2c0,35.6,0,71.2,0,106.8c0,10.8-0.1,21.6,0.1,32.4c0.1,4-1.4,5.4-5.3,5.2 c-4.8-0.2-9.7-0.4-14.5-0.1c-4.7,0.3-5.9-1.7-5.9-6.1c0.1-35.5,0.1-70.9,0.1-106.4C319.6,106.5,319.6,103.9,319.6,100.6z"}];
	  var grad = [
	    {"offset":"0%", "scolor":"#fff"},
	    {"offset":"0%", "scolor":"#fff"},
	    {"offset":"20%", "scolor":"#6B083F"},
	    {"offset":"40%", "scolor":"#fff"},
	    {"offset":"100%", "scolor":"#fff"}
	  ]

	  var svg = d3.select("#opot-logo").append("svg")
	      .attr("width", "100%")
	      .attr("height", "100%")
	      .attr("viewBox", "140 31.5 1000 298.5");

	  svg.append("rect").attr("x",0).attr("y", 0).attr("width", 1200).attr("height", 350).attr("clip-path", "url(#lights)").attr("fill", "url(#gradient)");

	  var defs = svg.append("defs");
	  var width = 1200;
	  var offw = 0;
	  var height = 325;
	  var offh = 0;

	  var vertices = d3.range(250).map(function(d,i) {
	    if(i === 0){
	      return [0,0];
	    }else if(i === 1){
	      return [1200,350];
	    }else if(i === 2){
	      return [0,350];
	    }else if(i === 3){
	      return [1200,0];
	    }else{
	      return [(Math.random() * 1000) + 200, (Math.random() * 275) + 50];
	    }
	  });
	  var base = d3.range(50).map(function(d,i) {
	    if(i === 0){
	      return [0,0];
	    }else if(i === 1){
	      return [1200,350];
	    }else if(i === 2){
	      return [0,350];
	    }else if(i === 3){
	      return [1200,0];
	    }else{
	      return [(Math.random() * 1000) + 200, (Math.random() * 275) + 50];
	    }
	  });

	  defs.append("clipPath").attr("id", "svgPath").selectAll("path")
	      .data(numbers).enter().append("path")
	      .attr("d", function(dt,i){ return dt.d; });

	  defs.append("clipPath").attr("id", "lights").selectAll("path")
	      .data(letters).enter().append("path")
	      .attr("d", function(dt,i){ return dt.d; });

	  defs.append("linearGradient").attr("id", "gradient")
	      .selectAll("stop").data(grad).enter().append("stop")
	      .attr("id", function(d,i){return "stop"+i;})
	      .attr("offset", function(d){return d.offset;})
	      .attr("stop-color", function(d){return d.scolor;});

	  var backnum = svg.append("g").attr("id","backnum").selectAll("path")
	      .data(numbers).enter().append("path")
	      .attr("d", function(dt,i){ return dt.d; });

	  var path = svg.append("g").attr("clip-path", "url(#svgPath)").selectAll("path");

	  var overtop = svg.append("g").attr("id", "letterOver").selectAll("path")
	                  .data(letters).enter().append("path")
	                  .attr("d", function(dt,i){ return dt.d; })
	                  .attr("fill", "rgba(0,0,0,0)");
	  redraw();

	  function redraw() {
	    var mod = Math.floor( Math.random() * 10 ) + 5;
	    backnum.classed("bright", true);
	    setTimeout(function(){
	      backnum.classed("bright", false);
	    }, 750);
	    vertices.forEach(function(d,i) {
	      if(i === 0){
	        return [0,0];
	      }else if(i === 1){
	        return [1200,350];
	      }else if(i === 2){
	        return [0,350];
	      }else if(i === 3){
	        return [1200,0];
	      }else if( i % mod === 0){
	        vertices[i] = [(Math.random() * 1000) + 200, (Math.random() * 275) + 50];
	      }
	    });
	    path = path.data(d3.geom.delaunay(vertices));
	    path.enter().append("path").attr("class", function(d, i) { return "q" + (i % 7); });
	    path.transition()
	        .delay(function(d,i){return (i % 10) * 100;})
	        .duration(1500).attr("d", function(d) { return "M" + d.join("L") + "Z"; });
	    setTimeout(function(){
	      redraw();
	    }, 5000);
	    setTimeout(function(){
	      shine();
	    }, 3000);
	  }
	  function shine() {
	    d3.selectAll("#stop1, #stop2, #stop3").transition().duration(3500).attr("offset", function(d){
	      var off = parseInt(d3.select(this).attr("offset").replace("%",""));
	      if(off > 50){
	        return (off-60)+"%";
	      }else{
	        return (off+60)+"%";
	      }
	    });
	  }  
	}
}
window.onLoad = init();