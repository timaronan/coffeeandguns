if ('addEventListener' in document) {
    document.addEventListener('DOMContentLoaded', function() {
        FastClick.attach(document.body);
    }, false);
}

var gunicon,cupicon,infowindow,geocoder,radius = 25,markers = [];
function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    scrollwheel: false,
    zoom: 9,
    center: {lat: 41.85197658, lng: -88.16309951},
    disableDefaultUI: true
  });

  map.set('styles', [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#2d3132"},{"lightness":0}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#252826"},{"lightness":0}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":0}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":0},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":0}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":0}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#2f2f2f"},{"lightness":0}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#2f2f2f"},{"lightness":0}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#666666"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#2f2f2f"},{"lightness":0}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#0e0e0e"},{"lightness":0}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#0e0e0e"},{"lightness":0},{"weight":1.2}]}]);

  geocoder = new google.maps.Geocoder();
  gunicon = {
      path: "M98.3,42l0.2-6.1l1.6,0c0.4,0,0.8-0.3,0.8-0.7l0.2-7.7c0-0.4-0.3-0.8-0.7-0.8l-1.6,0c0.1-1.5-1-2.6-2.2-2.6L95,24l0-1.1c0-0.4-0.3-0.8-0.7-0.8L90.6,22c-0.4,0-0.6,0.4-0.7,0.8l-0.4,1.1l-49.1-1.5c-0.6-0.4-1.1-0.8-1.6-1.1c-0.3-0.2-0.9-0.8-1.4-0.8l-6-0.2c-0.5,0-0.9,0.3-0.9,0.8l0,1.1L27.5,22c-1.3,0-2.3,1-2.4,2.2l-3.5,15.4c0,1.1,0.8,2.1,1.9,2.3l-0.1,0c0,0,5,0.7,4.4,4C19.2,69.2,21,74,21,74c0.2,1.2,1.2,2.1,2.3,2.2l21.2,0.6c1.2,0,2.3-0.9,2.6-2c0,0,0.9-6,3.9-17l14.2,0.4c2.2,0.1,4.3-0.6,5.7-1.9c1.9-1.8,2.4-4,3-5.9l1.5-6.8l20.5,0.6C97.2,44.2,98.2,43.2,98.3,42z M67.3,52.3c-0.3,0.2-0.5,0.4-1.7,0.4l-12.8-0.4c0.9-2.9,1.8-6,3-9.4l1.2,0c-0.1,1.9-0.1,5.6,1.7,7.7l1.2,0c0,0,0.2-5.1,3-7.5l7,0.2L68.7,49C68.1,50.9,67.6,52.1,67.3,52.3z",
      fillColor: '#C81A75',
      fillOpacity: .85,
      anchor: new google.maps.Point(0,0),
      strokeWeight: 0,
      scale: 0.15
  }
  cupicon = {
      path: "M24.1,26.8l7,47.5c4,1.4,8.8,2.5,14.1,2.5c5.4,0,9.7-0.9,14.3-2.5L66,26.9l0.2-1.7c1.9-0.3,3-0.7,3-1.1l-0.4-5.3c0-0.6-2.1-1.1-5.5-1.5l-0.3-3.8c0-1-8.1-1.9-18-1.9c-9.9,0-18,0.9-18,1.9l-0.3,3.9c-3.2,0.4-5.2,0.9-5.2,1.4l-0.4,5.3c0,0.4,0.9,0.7,2.4,1L24.1,26.8z",
      fillColor: '#1EA462',
      fillOpacity: .85,
      anchor: new google.maps.Point(0,0),
      strokeWeight: 0,
      scale: 0.15
  }
  infowindow =  new google.maps.InfoWindow({
    map: map
  });


  var toggle = d3.select('nav aside').append('div').attr("class", "toggle");
  d3.select("nav").classed("active", true);
  toggle.append('span').text("View U.S. Map");
  toggle.append('div').attr("class","btn").on('click', function(d,i){
     var txt = !d3.select('#map').classed('active') ? 'View U.S. Map' : 'View City Map';
     d3.select(".toggle span").text(txt);

      d3.select("nav").classed('active', !d3.select('#map').classed('active'));
      d3.select('#map').classed('active', !d3.select('#map').classed('active'));
      d3.select('#mapLayered').classed('active', !d3.select('#mapLayered').classed('active'));
    });

  console.log(starbucks.length, gunshops.length, gunshops.length/starbucks.length);          
  d3.select("#gunUS").text(gunshops.length);  
  d3.select("#starUS").text(starbucks.length);  
  document.getElementById('submit').addEventListener('click', function() {
    d3.select('body').classed('initiate', false);
    d3.select('#mapLayered').classed('active', false);
    d3.select('#map').classed('active', true);
    geocodeAddress(geocoder, map);
  });
}
function geocodeAddress(geocoder, resultsMap) {
  var address = document.getElementById('address').value;
  var starTot = document.getElementById('starTot');
  var gunTot = document.getElementById('gunTot');

  if(address === ""){
    address = "Chicago, IL";
  }
  
  geocoder.geocode({'address': address}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      deleteMarkers();
      resultsMap.setCenter(results[0].geometry.location);
      resultsMap.setZoom(9);

      var curlat = results[0].geometry.location.toJSON().lat;
      var curlng = results[0].geometry.location.toJSON().lng;
      var radStarbucks = buildRadius(curlat,curlng,starbucks,resultsMap,cupicon);
      var radGuns = buildRadius(curlat,curlng,gunshops,resultsMap,gunicon);
      var addr = 0;
      for (var i = 0; i < results[0].address_components.length; i++) {
        if(results[0].address_components[i].types[0] === "locality"){
          addr = i;
          break;
        }
      };
      addLocation(results[0].geometry.location,resultsMap,results[0].address_components[addr].short_name);
      
      starTot.textContent = radStarbucks;
      gunTot.textContent = radGuns;
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}
function showPosition(position) {
    x.innerHTML = "Latitude: " + position.coords.latitude + 
    "<br>Longitude: " + position.coords.longitude; 
}
function geocodeLatLng(geocoder, map, infowindow, obj) {
  // var input = document.getElementById('latlng').value;
  // var latlngStr = input.split(',', 2);
  // console.log(obj.toJSON(),obj.toJSON());r
  var objJ = obj.position.toJSON();
  var latlng = {lat: objJ.lat, lng: objJ.lng};
  geocoder.geocode({'location': latlng}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      if (results[1]) {
        infowindow.setContent(results[1].formatted_address);
        infowindow.open(map, obj);
      } else {
        console.log('No results found');
      }
    } else {
      console.log('Geocoder failed due to: ' + status);
    }
  });
}
function addLocation(location,map,city){
    d3.selectAll(".city .cur").text(city);
    var marker = new MarkerWithLabel({
      icon: {
        path: google.maps.SymbolPath.CIRCLE,
        scale: 0
      },
      draggable: false,
      labelAnchor: new google.maps.Point(10, 10),
      position: location,
      map: map,
      labelClass: "label"
    });
    markers.push(marker);
}
// Adds a marker to the map and push to the array.
function addMarker(location,icon,map,text) {
  var marker = new google.maps.Marker({
    position: location,
    map: map,
    animation: google.maps.Animation.DROP,
    icon: icon,
    labelClass: "label"
  });
  // marker.addListener('mouseover', function() {
  //   geocodeLatLng(geocoder, map, infowindow, this);
  // });
  // assuming you also want to hide the infowindow when user mouses-out
  marker.addListener('mouseout', function() {
    infowindow.close();
  });
  markers.push(marker);
}
// Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}
// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setMapOnAll(null);
}
// Shows any markers currently in the array.
function showMarkers() {
  setMapOnAll(map);
}
// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  markers = [];
}
function buildRadius(lat,lng,arr,map,icon){
  var results = [];
  var vicinity = [];
  for (var i = 0; i < arr.length; i++) {
    if(arr[i].lat < lat + 1.44 && arr[i].lat > lat - 1.44){
      results.push(arr[i]);
      var dist = distance(arr[i].lat, arr[i].lng, lat, lng);
      if(dist < radius){
        vicinity.push(arr[i]);
        var latlng = new google.maps.LatLng(arr[i].lat, arr[i].lng);
        var text;
        if(arr[i].name){
          text = arr[i].name;
        }else{
          text = "Starbucks";
        }
        text += ", " + arr[i].city + ", " + arr[i].state;
        addMarker(latlng,icon,map,text);
      }
    }
  };
  return vicinity.length;
}
function distance(lat1, lon1, lat2, lon2) {
  // p = Math.PI / 180
    var p = 0.017453292519943295;
    // localize cosine function
    var c = Math.cos;
    var a = 0.5 - c((lat2 - lat1) * p)/2 + 
          c(lat1 * p) * c(lat2 * p) * 
          (1 - c((lon2 - lon1) * p))/2;
  // Diameter = 2 * R; R of earth = 3959 miles
    return 7918 * Math.asin(Math.sqrt(a));
}

// totalsForShootings();
function totalsForShootings(){
  geojson.features.forEach(function(el, index){
      el.properties.starbucks = getRadius(el.geometry.coordinates[1],el.geometry.coordinates[0],starbucks);
      el.properties.gunshops = getRadius(el.geometry.coordinates[1],el.geometry.coordinates[0],gunshops);
      if(el.properties.starbucks > 0){
        var rat = el.properties.gunshops / el.properties.starbucks;
        var sb = "yes";
      }
      else{
        var rat = el.properties.gunshops;
        var sb = "no";
      }
      var diff = rat - 5.972;
      rat = Math.round(rat * 100) / 100;
      diff = Math.round(diff * 100) / 100;
      console.log(el.properties.city+","+el.properties.state+","+rat+","+diff+","+sb+","+el.properties.tot);
  });
  d3.select('body').append('p').text(JSON.stringify(geojson));
}
function getRadius(lat,lng,arr){
  var results = [];
  var vicinity = [];
  for (var i = 0; i < arr.length; i++) {
    if(arr[i].lat < lat + 1.44 && arr[i].lat > lat - 1.44){
      results.push(arr[i]);
      var dist = distance(arr[i].lat, arr[i].lng, lat, lng);
      if(dist < radius){
        vicinity.push(arr[i]);
      }
    }
  };
  return vicinity.length;
}