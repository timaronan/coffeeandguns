
R version 3.2.2 (2015-08-14) -- "Fire Safety"
Copyright (C) 2015 The R Foundation for Statistical Computing
Platform: x86_64-apple-darwin13.4.0 (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

[R.app GUI 1.66 (6996) x86_64-apple-darwin13.4.0]

[Workspace restored from /Users/michaelperez/.RData]
[History restored from /Users/michaelperez/.Rapp.history]

> myData <- read.csv("Downloads/fedgeo.csv", header=TRUE)
Warning message:
In scan(file, what, nmax, sep, dec, quote, skip, nlines, na.strings,  :
  EOF within quoted string
> street <- myData$street
> city <- myData$city
> named <- myData$named
> state <- myData$state
> lat <- myData$lat
> lng <- myData$lng
> df <- data.frame(street,city,named,state,lat,lng)
> write.csv(df, file="gunshops.csv", quote=FALSE, row.names=TRUE)
> fnl = df[order(-myData$city), ]
Warning message:
In Ops.factor(myData$city) : ‘-’ not meaningful for factors
> fnl = df[order(-df$city), ]
Warning message:
In Ops.factor(df$city) : ‘-’ not meaningful for factors
> fnl = df[order(df$city), ]
> write.csv(fnl, file="gs.csv", quote=FALSE, row.names=TRUE)
> dfll <- data.frame(lat,lng)
> write.csv(dfll, file="gunners.csv", quote=FALSE, row.names=TRUE)
> dff <- subset(dfll, lat > 0)
Warning message:
In Ops.factor(lat, 0) : ‘>’ not meaningful for factors
> dff <- subset(dfll, lat != '')
> write.csv(dff, file="gungun.csv", quote=FALSE, row.names=TRUE)
> dfs <- data.frame(state)
> dfs$total <- 1
> pp = aggregate(total ~ state, FUN = "sum", data = dfs)
> fnl = pp[order(-pp$total), ]
> write.csv(fnl, file="Downloads/coffeeandguns/gunshopsStates.csv", quote=FALSE, row.names=TRUE)
Error in file(file, ifelse(append, "a", "w")) : 
  cannot open the connection
In addition: Warning message:
In file(file, ifelse(append, "a", "w")) :
  cannot open file 'Downloads/coffeeandguns/gunshopsStates.csv': No such file or directory
> dfs$total <- 1
> pp = aggregate(total ~ state, FUN = "sum", data = dfs)
> fnl = pp[order(-pp$total), ]
> write.csv(fnl, file="Downloads/gunshopsStates.csv", quote=FALSE, row.names=TRUE)
> 